﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

	public Player player;

	public GameObject scoreAreaPrefab;
	public GameObject obstaclePrefab;
	public GameObject obstacleContainer;
	public FacebookScript facebookScript;

	public float holeSize;
	public float randomHoleSpacement;
	public float holeSpawnOffset;

	public float spawnDistance;
	public float spawnOffset;

	public Text titleText;
	public Text scoreText;
	public Text gameOverText;

	public Button playButton;
	public Button skinButton;

	private float spawnPointer = 0f;
	private int score = 0;
	private bool gameOver = false;


	// Use this for initialization
	void Start () {

		spawnPointer = -spawnDistance * 2;

		titleText.gameObject.SetActive (true);
		playButton.gameObject.SetActive (true);
		skinButton.gameObject.SetActive (true);

		scoreText.gameObject.SetActive (false);
		gameOverText.gameObject.SetActive (false);

		player.OnScore += OnScore;
		player.OnKill += OnKill;

	}

	// Update is called once per frame
	void Update () {

		if (player != null) {

			// Spawn new obstacles.
			if (spawnPointer - player.transform.position.x < spawnDistance) {
				spawnPointer += spawnDistance;

				SpawnObstacle (spawnPointer + spawnOffset);
			}

			// Clear new obstacle
			for (int i = 0; i < obstacleContainer.transform.childCount; i++) {

				GameObject currentObstacle = obstacleContainer.transform.GetChild (i).gameObject;

				if (player.transform.position.x - currentObstacle.transform.position.x > spawnOffset) {
					Destroy (currentObstacle);
				}
			}
		}

		if (gameOver == true) {

			if (Input.GetAxis ("Fire1") == 1f) {
				SceneManager.LoadScene ("Game");
			}

		}
	}
		
	void SpawnObstacle(float x) {

		float holeSpacement = Random.Range(-randomHoleSpacement, randomHoleSpacement);

		GameObject obstacleTop = Instantiate (obstaclePrefab);
		obstacleTop.transform.SetParent (obstacleContainer.transform);

		obstacleTop.transform.position = new Vector2 (
			x, holeSize / 2 + holeSpacement + holeSpawnOffset
		);

		// Rotate 180 grad the tube
		obstacleTop.transform.localEulerAngles = new Vector3 (0, 0, 180);

		GameObject obstacleBottom = Instantiate (obstaclePrefab);
		obstacleBottom.transform.SetParent (obstacleContainer.transform);

		obstacleBottom.transform.position = new Vector2 (
			x, -holeSize / 2 + holeSpacement + holeSpawnOffset
		);

		// Add Score trigger
		GameObject scoreArea = Instantiate (scoreAreaPrefab);
		scoreArea.transform.SetParent (obstacleContainer.transform);
		scoreArea.transform.position = new Vector2 (x, holeSpacement);

	}

	// Delegate Implementation

	void OnScore() {
		score++;
		scoreText.text = "Score: " + score;

		facebookScript.FacebookLogEvent("user_score","Best score", score);
	}

	void OnKill ()
	{
		scoreText.gameObject.SetActive (false);
		gameOverText.gameObject.SetActive (true);
		gameOverText.text = string.Format (gameOverText.text, score);
		gameOver = true;
	}

	public void onPlay() {
		
		titleText.gameObject.SetActive (false);
		playButton.gameObject.SetActive (false);
		skinButton.gameObject.SetActive (false);
		scoreText.gameObject.SetActive (true);

		player.StartMoving ();

		facebookScript.FacebookLogEvent("user_playButton","User play with skin", player.GetSkin());
	}

	public void onSkill() {

		player.CycleSkin ();
	}
}