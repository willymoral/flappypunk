﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

	public delegate void ScoreHandler();
	public delegate void KillHandler();

	public event ScoreHandler OnScore;
	public event KillHandler OnKill;


	public float jumpingSpeed = 5f;
	public float movingSpeed = 2f;
	public float verticalLimit;
	public float gravityScale;

	private Rigidbody2D playerRigidbody;
	private bool isMoving;
	private AudioSource flapSoundSource;
	private bool frozen = true;
	private int skinIndex = 0;


	// Use this for initialization
	void Start () {
		
		playerRigidbody = gameObject.GetComponent<Rigidbody2D> ();
		flapSoundSource = gameObject.GetComponent<AudioSource> ();

		playerRigidbody.gravityScale = 0;

		SetSkin (0);


	}
	
	// Update is called once per frame
	void Update () {

		// First Case - Keep the player within bounds
		if (transform.position.y > verticalLimit) {

			transform.position = new Vector3 (transform.position.x, verticalLimit, transform.position.z);
			playerRigidbody.velocity = new Vector2 (playerRigidbody.velocity.x, 0);

		} else if (transform.position.y < -verticalLimit) {

			Kill ();
		}

		// Make the player move vertically.
		if (frozen == false && Input.GetAxis ("Fire1") == 1f) {
			
			Flap ();
		}

		// Reset the rotation angle. 
		transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.identity, Time.deltaTime * 6);
	}

	void OnTriggerEnter2D(Collider2D otherCollider) {


		if (otherCollider.gameObject.tag == "Obstacle") {

			Kill ();

		} else if (otherCollider.gameObject.tag == "ScoreArea") {

			Destroy (otherCollider.gameObject);

			if (OnScore != null) {
				OnScore();
			}

		}
		
	}
		
	void Kill() {
		Destroy (gameObject);

		if (OnKill != null) {
			OnKill ();	
		}
	}

	void SetSkin(int index) {
		for (int i = 0; i < transform.childCount; i++) {
			transform.GetChild(i).gameObject.SetActive(i == index);
		}
	}
		
	void Flap() {

		playerRigidbody.velocity = new Vector2 (playerRigidbody.velocity.x, jumpingSpeed);

		transform.rotation = Quaternion.Euler (0, 0, 35);

		flapSoundSource.Play ();
	}
		
	public void StartMoving() {

		frozen = false;

		Flap();

		// Horizontal Velocity
		playerRigidbody.velocity = new Vector2 (movingSpeed, playerRigidbody.velocity.y);
		playerRigidbody.gravityScale = gravityScale;
	}

	public void CycleSkin() {
		skinIndex++;

		if (skinIndex >= transform.childCount) {
			skinIndex = 0;	
		}

		SetSkin (skinIndex);
	}

	public int GetSkin() {
		return skinIndex;
	}
}